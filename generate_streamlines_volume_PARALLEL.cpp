// GENERATES STREAMLINES USING MEDUS AND MARKS NODES LYING OUTSIDE
// OF THE PERCOLATING STREAMLINES
// TODO:

#include <iostream>
#include <fstream>
#include <string>

#include <omp.h>

// MEDUSA
#include <utils/assert.cpp>
#include <medusa/Medusa.hpp>

typedef mm::Vec3d T;
typedef mm::RBFFD<mm::Polyharmonic<double,3>, T> T_appr;

// THE START AND THE END OF THE POROUS SAMPLE IN THE DOMAIN
double x_porous_0 = 20., x_porous_1 = 36.;

#include "filtering_functions.h"
#include "meshless_streamlines.h"
#include "seeding_functions.h"
#include "streamlines_set_manipulation.h"
#include "saving.h"

int main(int argc, char* argv[]){

    // SET THE NUMBER OF THREADS TO BE USED
    omp_set_num_threads(11);

    // USEFUL VARIABLES
    int m=2; // INTERPOLATION AUGMENTATION ORDER
    int every = atof(argv[2]); // EVERY WHICH DISCRETIZATION POINT TO USE AS A STREAMLINE SEED
    double h = atof(argv[2]); // VOLUME SAMPLING SPACING
    double dt = atof(argv[3]); // TIMESTEP LENGTH
    std::string res_path = "sl_parallel/"; // RESULTS FOLDER TO SAVE TO

    // CREATE THE DOMAIN
    std::vector<T> V{}, V_norm{}, P_vec{}; // VELOCITY, VELOCITY NORMALIZED TO UNIT, DISCRETIZATION POINTS
    std::vector<T> bbox = populate_domain<T>(P_vec,V,V_norm,argv[1]); // READS THE DISCRETIZATION AND VELOCITIES
    mm::KDTree<T> P_kdt(P_vec); // A KD-TREE FOR FASTER STENCIL SEARCH

    // DECLARE THE ARRAY OF STREAMLINES' STARTING POSITIONS
    std::vector<T> p0{};
    
    // INITIALIZE STREAMLINES AT EACH DISCRETIZATION POINT
    // seed_from_discretization(P_vec,V,p0,every);
    // INITIALIZE STREAMLINES IN A BOX VOLUME CONFINED BETWEEN x0 AND x1
    T x0(22,6,6), x1(28,10,10);
    seed_box(x0,x1,V,p0,h,P_kdt,V);

    // POPULATE STREAMLINES STARTING POINTS ARRAYS
    std::vector<std::vector<T>> sl0 = initialize_streamlines(p0); // FORWARD-INTEGRATED STREAMLINES
    std::vector<std::vector<T>> sl1 = initialize_streamlines(p0); // BACKWARD-INTEGRATED STREAMLINES

    // GENERATE_STREAMLINES
    generate_streamlines_PARALLEL(P_kdt,V_norm,sl0,dt); // FORWARD-INTEGRATED STREAMLINES
    generate_streamlines_PARALLEL(P_kdt,V_norm,sl1,-dt); // BACKWARD-INTEGRATED STREAMLINES
    // MERGE FORWARD AND BACKWARD STREAMLINES
    join_streamlines_PARALLEL(sl0,sl1);

    // MARK STREAMLINES AS PERCOLATING, NON-PERCOLATING, HAVING NEGATIVE VELOCITY ETC.
    std::vector<int> type = mark_streamlines_PARALLEL(sl0,dt);

    // FIND WHICH POINTS OF THE STREAMLINES' INITIAL POINTS LIE IN VORTICES
    // std::vector<T> vp = find_vortex_points_by_distance(sl0,P_vec,type,h);
    std::vector<T> vp = find_vortex_points_by_streamlines(sl0,p0,type);

    // SAVE STREAMLINES (INCLUDES A CONDITIONAL WHICH STREAMLINES TO ACTUALLY SAVE)
    save_streamlines_PARALLEL(sl0,type,res_path,1);

    // SAVE VORTEX POINTS
    save_vortex_points(vp,res_path+"vortex_points.csv");

}