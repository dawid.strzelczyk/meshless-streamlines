# Meshless Streamlines

A few headers to generate streamlines with masless tracers usgin meshless interpolation of [Medusa library](https://e6.ijs.si/medusa/). The compilation relies on [the standalone](https://e6.ijs.si/medusa/wiki/index.php/Including_this_library_in_your_project) version of Medusa, so adjust the linker accordingly, e.g.:

```
compile () {
	f=$1
	g++ -fopenmp -o $f $f.cpp -I ~/medusa/src -I ~/medusa/include -I /usr/include/hdf5/serial -L ~/medusa/bin/ -lmedusa_standalone -O3
}
```

Uses multithreading via OMP.

## Contents

- ```meshless_streamlines.h``` - the core of the method, contains the routines to interpolate and advance hte solution in time, marking streamlines as percolating, non-percoalting, having negative velocities, etc.
- ```filtering_functions.h``` - a bunch of methods to test if a given streamine is percolating, has negative velocity, etc.
- ```seeding_functions.h``` - methods for spawning the initial points for streamlines
- ```streamlines_set_manipulation.h``` - methods for stitching forward/backward streamlines started from the same point, joining two sets of streamlines, etc.
- ```generate_streamlines_volume_PARALLEL.cpp``` - an exemplary code to generate streamlines (see comments in the code)

Parallel methods are denoted by the suffix ```_PARALLEL```, single threaded ones don't have any suffix.

## Usage

- **Input:** a file with $2 \times d$ columns (points coordinates, velocities $\rightarrow x_0,x_1,x_2,V_0,V_1,V_2$), where $d$ is the dimensionality of the domain. Columns should be separated by a space or tab and contain no headers.
- **Output:** files with streamlines points (each streamline is one file, so be careful, since for a large number of SLs it gets heavy), a file with points marked as vortex points

## Example result
![sl](streamlines_0.png "")

## Project status
So far tested on 3D domains only...
