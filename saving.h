#ifndef SAVING_H
#define SAVING_H

// SAVES EVERY every STREAMLINE OF THE CHOSEN TYPE; ONE STREAMLINE = ONE FILE
void save_streamlines_PARALLEL(std::vector<std::vector<T>>& sl, std::vector<int>& type, std::string file, int every_sl=1, int every_point=1){
    std::cout<<"Saving streamlines..."<<std::endl;
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        int p = omp_get_num_threads();

        for (int i=id;i<sl.size();i+=p*every_sl){
            std::vector<T> sl_now = sl[i];
            // CHOOSE THE STREAMLINE TYPE
            if (type[i]>=1){
                std::ofstream out(file+"sl_"+std::to_string(i)+".dat");
                // WRITE THE TYPE IN THE FIRST ROW
                out << type[i] << std::endl;
                for (int k=0;k<sl_now.size();k+=every_point){
                    T P_now = sl_now[k];
                    // CHECK NaNs
                    if (std::isnan(P_now(0)) || std::isnan(P_now(1)) || std::isnan(P_now(2)))
                        break;
                    for (int j=0;j<P_now.size();j++)
                        out << P_now(j) << " ";
                    out << std::endl;
                }
                out.close();
            }
        }
    }
}


void save_streamlines(std::vector<std::vector<T>>& sl, std::vector<int>& type, std::string file){
    for (int i=0;i<sl.size();i++){
        std::cout<<"Saving streamlines..."<<std::endl;
        std::vector<T> sl_now = sl[i];
        if (type[i]>1){
            std::ofstream out(file+"sl_"+std::to_string(i)+".dat");
            out << type[i] << std::endl;
            for (int k=0;k<sl_now.size();k+=1){
                T P_now = sl_now[k];
                if (std::isnan(P_now(0)) || std::isnan(P_now(1)) || std::isnan(P_now(2)))
                    break;
                for (int j=0;j<P_now.size();j++)
                    out << P_now(j) << " ";
                out << std::endl;
            }
            out.close();
        }
    }
}

// SAVE VORTEX POINTS TO ONE FILE
template<typename T>
void save_vortex_points(std::vector<T>& vp, std::string file){
    std::cout<<"Saving vortex points..."<<std::endl;
    std::ofstream out(file);
    for (T p : vp)
        out << p(0) << ", " << p(1) << ", " << p(2) << std::endl;
    out.close();
}

#endif