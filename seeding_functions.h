#ifndef SEEDING_FUNCTIONS_H
#define SEEDING_FUNCTIONS_H

// READS A BOX TO SEED THE STREAMLINES STARTING POINTS IN FROM A FILE;
// E.G. "0 0 0 1 1 1" WILL GIVE A UNIT CUBE WITH THE CENTER AT (.5,.5,.5)
void read_box(std::string file, T& x0, T& x1){
    std::ifstream in(file);
    if (in.is_open()){
        std::cout<<"Opened box file "<<file<<std::endl;
        double temp;
        for(int i=0;i<3;i++){
            in >> temp;
            x0(i)=temp;
        }
        for(int i=0;i<3;i++){
            in >> temp;
            x1(i)=temp;
        }
        in.close();
    }
}

// SEEDS ON A PLANE
template<typename T>
void seed_inlet_plane(double x_plane,std::vector<T>&bbox,std::vector<T>&p0,double h){
    // SEED ON A REGULAR GRID
    T p_now = T(x_plane,7.5,h);
    while (p_now(1)<8.5){
        while (p_now(2)<bbox[1](2)){
            p0.push_back(p_now);
            p_now(2)+=h;
        }
        p_now(1)+=h;
        p_now(2) = bbox[0](2);
    }
    // std::cout << "Inserted the following seed points:" <<std::endl;
    // for (T p : p0)
    //     std::cout << p << std::endl;
    std::cout << "Done seeding " << p0.size() << " points." << std::endl;
}

// SEEDS INTO A BOX CONFINED WITHINF x0 AND x1
template<typename T>
void seed_box(T x0,T x1,std::vector<T>&bbox,std::vector<T>&p0,double h,mm::KDTree<T>& P_kdt,std::vector<T>&V){
    // SEED ON A REGULAR GRID
    T p_now = x0;
    while (p_now(0)<x1(0)){
        while (p_now(1)<x1(1)){
            while (p_now(2)<x1(2)){
                int i_closest = P_kdt.query(p_now,1).first[0];
                double vmag_closest = V[i_closest].norm();
                if (vmag_closest > 1e-10)
                    p0.push_back(p_now);
                p_now(2)+=h;
            }
            p_now(1)+=h;
            p_now(2) = x0(2);
        }
        p_now(0)+=h;
        p_now(1) = x0(1);
    }
    std::cout << "Done seeding " << p0.size() << " points." << std::endl;
}

// USES THE DISCRETIZATION POINTS A STHE STREAMLINES STARTING POINTS
template<typename T>
void seed_from_discretization(std::vector<T>& P, std::vector<T>& V, std::vector<T>&p0, int every=1){
    std::cout<<"Seeding initial points..."<<std::endl;
    for (int i=0;i<P.size();i+=every){
        bool is_in_band = (P[i](1)>=7.5 && P[i](1)<=8.5);
        if (P[i](0) >= x_porous_0 && P[i](0) <= x_porous_1 && V[i].norm()>1e-12 && is_in_band)
            p0.push_back(P[i]);
    }
    std::cout << "Done seeding " << p0.size() << " points." << std::endl;
}

#endif