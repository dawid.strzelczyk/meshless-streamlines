#ifndef STREAMLINES_SET_MANIPULATION_H
#define STREAMLINES_SET_MANIPULATION_H

// STICHES THE FORWARD AND BACKWARD INTEGRATED STREAMLINES TOGETHER (WARNING: INDEXES IN sl_* ARRAYS
// MUST MATCH!)
template<typename T>
void join_streamlines(std::vector<std::vector<T>>& sl_plus, std::vector<std::vector<T>>& sl_minus){
    std::cout << "Joining forward and backward streamlines (single-threaded)..." << std::endl;
    T test(.0,.0,.0);
    for (int i=0;i<sl_plus.size();i++){
        if (sl_plus[i].size()>1 && sl_minus[i].size()>1){
            sl_minus[i].erase(sl_minus[i].begin());
            for (int j=0;j<sl_minus[i].size()-1;j++){
                sl_plus[i].emplace(sl_plus[i].begin(),sl_minus[i][j]);
            }
        }
    }
}

// STICHES THE FORWARD AND BACKWARD INTEGRATED STREAMLINES TOGETHER (WARNING: INDEXES IN sl_* ARRAYS
// MUST MATCH!)
template<typename T>
void join_streamlines_PARALLEL(std::vector<std::vector<T>>& sl_plus, std::vector<std::vector<T>>& sl_minus){
    std::cout << "Joining forward and backward streamlines (multi-threaded)..." << std::endl;
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        int p = omp_get_num_threads();
        
        for (int i=id;i<sl_plus.size();i+=p){
            if (sl_plus[i].size()>1 && sl_minus[i].size()>1){
                sl_minus[i].erase(sl_minus[i].begin());
                for (int j=0;j<sl_minus[i].size()-1;j++){
                    sl_plus[i].emplace(sl_plus[i].begin(),sl_minus[i][j]);
                }
            }
        }
    }
}

// JOINS TWO STREAMLINES SETS (E.G. WHEN ONE IS SEEDED BEFORE INLET AND FORWARD INTEGRATED AND THE OTHER
// IS SEEDED AFTER THE OUTLET AN IS BACKWARD-INTEGRATED)
template<typename T>
void add_sl_sets(std::vector<std::vector<T>>& sl0, std::vector<std::vector<T>>& sl1){
    std::cout << "Joining streamlines sets..." << std::endl;
    for (std::vector<T> sl_now : sl1){
            std::vector<T> sl_now_reversed{};
            for (T p : sl_now)
                sl_now_reversed.emplace(sl_now_reversed.begin(),p);
            sl0.push_back(sl_now_reversed);
    }
}

#endif