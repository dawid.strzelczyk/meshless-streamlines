#ifndef MESHLESS_STREAMLINES_H
#define MESHLESS_STREAMLINES_H

// LOAD A FILE WITH POINTS COORDINATES AND VELOCITIES AND POPULATE THE POINTS AND VELOCITIES ARRAYS
// REURNS THE BOUNDING BOX OF THE DOMAIN
template<typename T>
std::vector<T> populate_domain(std::vector<T>& P,std::vector<T>& V, std::vector<T>& V_norm,std::string file, bool is_2d=false){
    T x0,x1;
    int d = is_2d ? 2 : x0.size();
    std::cout<<"d="<<d<<std::endl;

    std::ifstream in(file);
    if (in.is_open()){
        std::cout << "Opened input file " << file << std::endl;
        T p,v;
        for (int i=0;i<p.size();i++){
            x0(i)=1e10;
            x1(i)=-1e10;
        }
        while (true){
            for (int i=0;i<d;i++)
                in >> p(i);
            if (is_2d)
                p(2)=.0;
            
            if (in.eof())
                break;
            if (p(0) >= x_porous_0-1.5 && p(0) <= x_porous_1+1.5){
                P.push_back(p);
                for (int i=0;i<p.size();i++){
                    if (p(i)<x0(i))
                        x0(i)=p(i);
                    if (p(i)>x1(i))
                        x1(i)=p(i);
                }

                for (int i=0;i<d;i++)
                    in >> v(i);
                if (is_2d)
                    v(2)=.0;
                V_norm.push_back(v/(v.norm()+1e-12));
                V.push_back(v);
            }
        }
    }
    else{
        std::cout << "Couldn't open input file " << file << std::endl;
        return std::vector<T>{T(),T()};
    }
    std::cout << "Read " << P.size() << " points with " << V.size() << " velocities" << std::endl;
    std::cout << "Domain contained within:" << std::endl << x0 << std::endl << x1 << std::endl;
    in.close();
    if (is_2d){
        x0(2)=-1.;
        x1(2)=1.;
    }
    return std::vector<T>{x0,x1};
}

// CREATES THE ARRAY OF STREAMLINES GIVEN THE INITIAL POSITIONS ARRAY
template<typename T>
std::vector<std::vector<T>> initialize_streamlines(std::vector<T> p){
    std::vector<std::vector<T>> ret{};
    for (int i=0;i<p.size();i++)
        ret.push_back(std::vector<T>{p[i]});
    return ret;
}

// TIME INTEGRATION OF THE STREAMLINES ACCORDING TO THE INTERPOLATED VELOCITY FIELD
template<typename T, typename T_appr>
void generate_streamlines(mm::KDTree<T>& P, std::vector<T>& V, T_appr& appr, std::vector<std::vector<T>>& sl, double dt=1e-1, int nj=25){
    double x_end = x_porous_1+.1;
    double x_start = x_porous_0-.1;
    for (int a=0;a<sl.size();a++){
        std::vector<T>& sl_now = sl[a];
        int iter=0;
        // std::cout << "Processing streamline " << sl_now[0] << std::endl;
        bool reached_end = false;
        while (!reached_end && iter<16*2/std::abs(dt)){
            if (std::isnan(sl_now[sl_now.size()-1](0)) || std::isnan(sl_now[sl_now.size()-1](1)) || std::isnan(sl_now[sl_now.size()-1](2)))
                    break;
            // std::cout<<sl_now[sl_now.size()-1]<<",";
            // UPDATE THE SUPPORT OF THE CURRENT STREAMLINE END
            std::vector<int> i_support = P.query(sl_now[sl_now.size()-1],nj).first;
            std::vector<T> support{};
            for (int i:i_support)
                support.push_back(P.get(i));
            // SET THE APPROXIMATION ENGINE TO THE CURRENT STREAMLINE POINT AND SUPPORT
            appr.compute(sl_now[sl_now.size()-1],support);
            // GET THE INTERPOLATION SHAPE VECTOR
            Eigen::MatrixXd shape = appr.getShape();
            // APPROXIMATE THE VELOCITY AT THE CURRENT STREAMLINE END
            T V_now;
            for (int i=0;i<V_now.size();i++){
                V_now(i)=.0;
                for (int j=0;j<support.size();j++)
                    V_now(i) += shape(j) * V[i_support[j]](i);
            }
            // GENERATE AN INCREMENT OF THE STREAMLINE
            T dx = dt*V_now;
            if (dx.norm() > 2*std::abs(dt))
                break;
            sl_now.push_back(sl_now[sl_now.size()-1] + dx);
            // std::cout << sl_now[sl_now.size()-1] << std::endl;
            iter++;
            if (dt>0){
                if (sl_now[sl_now.size()-1](0)>x_end)
                    reached_end = true;
            }
            else{
                if (sl_now[sl_now.size()-1](0)<x_start)
                    reached_end = true;
            }
        }
        // std::cout << "Processed streamline " << sl_now[0] << std::endl;
    }
    std::cout << "Done generating " << sl.size() << " streamlines." << std::endl;
}

// TIME INTEGRATION OF THE STREAMLINES ACCORDING TO THE INTERPOLATED VELOCITY FIELD
template<typename T>
void generate_streamlines_PARALLEL(mm::KDTree<T>& P, std::vector<T>& V, std::vector<std::vector<T>>& sl, double dt=1e-1, int nj=25, int m=2){
    std::cout<<"Generating streamlines..."<<std::endl;
    double x_end = x_porous_1+.1;
    double x_start = x_porous_0-.1;
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        int p = omp_get_num_threads();
        
        mm::Polyharmonic<double,3> ph;
        T_appr appr(ph, mm::Monomials<T>(m));
        
        for (int a=id;a<sl.size();a+=p){
            std::vector<T>& sl_now = sl[a];
            int iter=0;
            // std::cout << "Processing streamline " << sl_now[0] << std::endl;
            bool reached_end = false;

            while (!reached_end && iter<16*1.5/std::abs(dt)){
            // while (!reached_end && iter<2/std::abs(dt)){
                // CHECK IF THE LAST POINT IS NOT NaN
                if (std::isnan(sl_now[sl_now.size()-1](0)) || std::isnan(sl_now[sl_now.size()-1](1)) || std::isnan(sl_now[sl_now.size()-1](2)))
                        break;
                // GET THE SUPPORT OF THE CURRENT STREAMLINE END
                std::vector<int> i_support = P.query(sl_now[sl_now.size()-1],nj).first;
                std::vector<T> support{};
                for (int i:i_support)
                    support.push_back(P.get(i));
                // SET THE APPROXIMATION ENGINE TO THE CURRENT STREAMLINE POINT AND SUPPORT
                appr.compute(sl_now[sl_now.size()-1],support);
                // GET THE INTERPOLATION SHAPE VECTOR
                Eigen::MatrixXd shape = appr.getShape();
                // APPROXIMATE THE VELOCITY AT THE CURRENT STREAMLINE END
                T V_now;
                for (int i=0;i<V_now.size();i++){
                    V_now(i)=.0;
                    for (int j=0;j<support.size();j++)
                        V_now(i) += shape(j) * V[i_support[j]](i);
                }
                // GENERATE AN INCREMENT OF THE STREAMLINE (SIMPLE EULER SO FAR...)
                T dx = dt*V_now;
                if (dx.norm() > 2*std::abs(dt))
                    break;
                sl_now.push_back(sl_now[sl_now.size()-1] + dx);

                iter++;
                
                if (dt>0){
                    if (sl_now[sl_now.size()-1](0)>x_end)
                        reached_end = true;
                }
                else{
                    if (sl_now[sl_now.size()-1](0)<x_start)
                        reached_end = true;
                }
            }
            // std::cout << "Processed streamline " << sl_now[0] << std::endl;
        }
    }
    std::cout << "Done generating " << sl.size() << " streamlines." << std::endl;
}

// RETURNS AN ARRAY OF STREAMLINES TYPE; EACH TYPE IS ASSOCIATED WITH DIFFERENT KIND OF A STREAMLINE (PERCOLATING, HAVING NEGATIVE VELOCITY, ETC.)
template<typename T>
std::vector<int> mark_streamlines(std::vector<std::vector<T>>& sl, double dt){
    std::cout << "Cleaning up streamlines..." << std::endl;
    std::vector<int> ret{};
    for (int i=0;i<sl.size();i++){
        std::vector<T>& sl_now = sl[i];
        // CHECK IF SIZE>0
        bool b0 = sl_now.size()>0 && !cuts_border(sl_now);
        // CHECK IF SL PERCOLATES
        bool b1 = percolates(sl_now);
        // CHECK IF IT LOOPS (GETS TRAPPED IN A VORTEX)
        // bool b2 = loops_total_length(sl_now,dt);
        // CHECK NEGATIVE u-VELOCITIES
        bool b2 = has_negative_vel(sl_now);
        
        if (!b0)
            ret.push_back(0);
        else if (b0 && !b1)
            ret.push_back(1);
        else if (b0 && b1 && !b2)
            ret.push_back(2);
        else if (b0 && b1 && b2)
            ret.push_back(3);
    }
    return ret;
}

// RETURNS AN ARRAY OF STREAMLINES TYPE; EACH TYPE IS ASSOCIATED WITH DIFFERENT KIND OF A STREAMLINE (PERCOLATING, HAVING NEGATIVE VELOCITY, ETC.)
template<typename T>
std::vector<int> mark_streamlines_PARALLEL(std::vector<std::vector<T>>& sl, double dt, std::vector<T>& bbox){
    std::cout << "Cleaning up streamlines..." << std::endl;
    std::vector<int> ret(sl.size(),0);
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        int p = omp_get_num_threads();
        for (int i=id;i<sl.size();i+=p){
            std::vector<T>& sl_now = sl[i];
            // CHECK IF SIZE>0
            bool b0 = sl_now.size()>0 && !cuts_border(sl_now,bbox[0],bbox[1]);
            // CHECK IF SL PERCOLATES
            // bool b1 = percolates(sl_now);
            bool b1 = true;
            // CHECK IF IT LOOPS (GETS TRAPPED IN A VORTEX)
            // bool b2 = loops_total_length(sl_now,dt);
            // CHECK NEGATIVE u-VELOCITIES
            // bool b2 = has_negative_vel(sl_now);
            bool b2 = has_negative_vel_by_angle(sl_now,155);
            if (!b0)
                ret[i]=0;
            else if (b0 && !b1)
                ret[i]=1;
            // THE MOST USEFUL CRITERION - SL WITH NEGATIVE VELOCITIES
            else if (b0 && b1 && !b2)
                ret[i]=2;
            else if (b0 && b1 && b2)
                ret[i]=3;
        }
    }
    return ret;
}

// RETURNS AN ARRAY OF STREAMLINES TYPE; EACH TYPE IS ASSOCIATED WITH DIFFERENT KIND OF A STREAMLINE (PERCOLATING, HAVING NEGATIVE VELOCITY, ETC.)
template<typename T>
std::vector<int> mark_streamlines_new_PARALLEL(int(*test_function)(std::vector<T>,std::vector<T>), std::vector<std::vector<T>>& sl, double dt, std::vector<T>& bbox){
    std::cout << "Cleaning up streamlines..." << std::endl;
    std::vector<int> ret(sl.size(),0);
    #pragma omp parallel
    {
        int id = omp_get_thread_num();
        int p = omp_get_num_threads();
        for (int i=id;i<sl.size();i+=p){
            std::vector<T>& sl_now = sl[i];
            ret[i]=test_function(sl_now,bbox);
        }
    }
    return ret;
}

// RETURNS AN ARRAY OF POINTS LYING IN VORTICES; I.E. THIS IS THE ARRAY OF THE DISCRETIZATION POINTS LYING FURTHER THAN SOME DISTANCE FROM PERCOALTING
// NON-VORTEX STREAMLINES (WARNING, METHOD IS OBSOLETE!)
template<typename T>
std::vector<T> find_vortex_points_by_distance(std::vector<std::vector<T>>& sl, std::vector<T>& P_vec, std::vector<int>& type, double h){
    std::cout << "Finding vortex points by distances..." << std::endl;
    std::vector<T> ret{};
    // CREATE A KD-TREE OF STREAMLINES POINTS
    std::vector<T> sl_vec{};
    for (int i=0;i<sl.size();i++){
        std::vector<T>& sl_now = sl[i];
        if (type[i]==2)
            for (T p_now : sl_now)
                sl_vec.push_back(p_now);
    }
    mm::KDTree<T> sl_kdt(sl_vec);
    // FIND THE CLOSEST SL POINT FOR EACH DOMAIN POINT
    for (T p_now : P_vec){
        double d = sqrt(sl_kdt.query(p_now,1).second[0]);
        // CHECK IF THE CONSIDERED POINT IS IN THE POROUS SAMPLE
        bool p_now_in_porous = (p_now(0) >= x_porous_0 && p_now(0) <= x_porous_1);
        bool p_now_in_band = (p_now(1) >= 7.5 && p_now(1) <= 8.5);
        if (d > h && p_now_in_porous && p_now_in_band)
            ret.push_back(p_now);
    }
    std::cout << "Found " << ret.size() << " vortex points" << std::endl;
    return ret;
}

// RETURNS AN ARRAY OF POINTS LYING IN VORTICES; I.E. THIS IS THE ARRAY OF STREAMLINES' INITIAL POINTS THAT PASSED THE CHOSEN TESTS IN mark_streamlines()
template<typename T>
std::vector<T> find_vortex_points_by_streamlines(std::vector<std::vector<T>>& sl, std::vector<T>& p0, std::vector<int>& type){
    std::cout << "Finding vortex points by streamlines..." << std::endl;
    if (sl.size() != p0.size()){
        std::cout << "Unequal number of streamlines ("<<sl.size()<<") and initial points ("<<p0.size()<<")."<<std::endl;
        // return std::vector<T>{};
    }
    std::vector<T> ret{};
    for (int i=0;i<sl.size();i++){
        if (type[i] == 3)
            ret.push_back(p0[i]);
    }
    return ret;
}

#endif