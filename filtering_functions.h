#ifndef FILTERING_FUNCTIONS_H
#define FILTERING_FUNCTIONS_H

// CHECKS IF A SL PERCOALTES BASED ON THE POSITIONS OF THE FIRST/LAST SL'S POINT
template<typename T>
bool percolates(std::vector<T> sl){
    double x_end = x_porous_1;
    double x_start = x_porous_0;
    if (sl[sl.size()-1](0) > x_end && sl[0](0) < x_start)
        return true;
    return false;
}

// CHECKS THE LOOP BASED ON THE MASS OF A SL IN A BOX OF x-WIDTH EQUAL TO
// window_size
template<typename T>
bool loops_segment_length(std::vector<T>& sl, double dt){
    int n = 5;
    double window_size = n*dt, x_now=x_porous_0;
    while (x_now+window_size<=x_porous_1){
        int mass=0;
        for (T p_sl : sl){
            if (p_sl(0)>=x_now && p_sl(0)<=x_now+window_size)
                mass++;
        }
        if (mass > 4*n)
            return true;
        x_now += window_size;
    }
    return false;
}

// CHECKS THE LOOP BASED ON THE TOTAL LENGTH OF A SL
template<typename T>
bool loops_total_length(std::vector<T>& sl, double dt){
    int n=0;
    for (T p : sl)
        if (p(0)>=x_porous_0 && p(1)<=x_porous_1)
            n++;
    if (n<1.4 * 16./dt)
        return false;
    return true;
}

// CHECKS IF A STREAMLINE FALLS OUT OF THE DOMAIN BORDERS (FROM 0 TO 16)
template<typename T>
bool cuts_border(std::vector<T>& sl, T x0, T x1){
    for (T p : sl){
        bool ret=false;
        if (p(1)<x0(1) || p(1)>x1(1) || p(2)<x0(2) || p(2)>x1(2))
            return true;
    }
    return false;
}

// CHECKS IF A SL HAS NEGATIVE VELOCITY BASED ON THE V_0 COMPONENT SIGN
template<typename T>
bool has_negative_vel(std::vector<T>& sl){
    int n=0;
    for (int i=1;i<sl.size();i++){
        double du = sl[i](0) - sl[i-1](0);
        if (du<.0)
            return true;
    }
    return false;
}

// CHECKS IF A SL HAS NEGATIVE VELOCITY BASED ON ITS ANGLES IN xz AND xy PLANE
// +/-phi IS A THERSHOLD ANGLE ABOVE WHICH THE STREAMLINE IS CONSIDERED AS
// FLOWING BACKWARD
template<typename T>
bool has_negative_vel_by_angle(std::vector<T>& sl,double phi){
    int n=0;
    phi *= M_PI/180.;
    for (int i=1;i<sl.size();i++){
        T du = sl[i] - sl[i-1];
        // if (du.size()==3 && std::abs(std::atan2(du(1),du(0))) > phi && std::abs(std::atan2(du(2),du(0))) > phi){
        if (std::acos(du(0)/du.norm()) > phi)
            return true;
    }
    return false;
}

#endif